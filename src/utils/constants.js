export const START_VIEW_TRANSITION = 'start-view-transition';
export const SHOW_NOTIFICATION = 'show-notification';
export const MOBILE_BREAKPOINT = 768;
export const COOKIES_ACCEPTED = 'cookies-accepted';
// export const API_URL = 'http://localhost:3001'
export const API_URL = 'https://localhost:44347'
export const SITE_NAME = 'Props Tracker';
export const DEVICE_TOKEN = 'device-token'
export const SESSION_KEY = 'session-key'

export const STATUS = {
  SEND: "send",
  AUTHORIZED: "authorized",
  REJECTED: "rejected",
};

export const USER_TYPES = {
  ADMIN: "admin",
  PROMOTOR: "promotor"
};

