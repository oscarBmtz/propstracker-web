import moment from 'moment';
import _ from 'lodash';
import 'moment/locale/es';

const formatter = {
  date(date, formatString = 'D MMMM YYYY') {
    if (!date) return '';

    moment.locale('es');
    return moment(date).format(formatString);
  },
  capitalize(text = '', type) {
    if (!text) return '';

    if (text && !text.length) {
      return text;
    }

    if (text.length === 1) {
      return text.toUpperCase();
    }

    if (type === 'sub') {
      return text;
    }

    const firstLetter = text.substring(0, 1);
    const remainingText = text.substring(1);
    return `${firstLetter.toUpperCase()}${remainingText.toLowerCase()}`;
  },
  clean(object) {
    const result = {};
    const objectAttributes = Object.keys(object);

    objectAttributes.forEach((attribute) => {
      const attributeValue = object[attribute];

      if (_.isPlainObject(attributeValue)) {
        result[attribute] = formatter.clean(attributeValue);
      } else if (!_.isFunction(attributeValue)) {
        result[attribute] = attributeValue;
      }
    });

    return result;
  }
};


export default formatter;
