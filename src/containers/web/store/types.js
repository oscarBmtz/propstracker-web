export const LOGIN_USER = 'web/LOGIN_USER';
export const SET_LOADING = 'web/SET_LOADING';
export const REGISTER_USER = 'web/REGISTER_USER';
export const SET_REGISTER_DATA = 'web/SET_REGISTER_DATA';
export const SET_SELECTED_VIEW = 'web/SET_SELECTED_VIEW';
export const SET_HEADER_VISIBILITY = 'web/SET_HEADER_VISIBILITY';
export const SET_USER_SESSION = 'web/SET_USER_SESSION';
export const SET_USER_DATA = 'web/SET_USER_DATA';


