import * as actionsTypes from './types';


export const setHeaderVisibility = (params) => ({
  type: actionsTypes.SET_HEADER_VISIBILITY,
  payload: params,
});

export const setUserSession = (params) => ({
  type: actionsTypes.SET_USER_SESSION,
  payload: params,
});

export const setSelectedView = (params) => ({
  type: actionsTypes.SET_SELECTED_VIEW,
  payload: params,
});

export const userLogin = (params) => ({
  type: actionsTypes.LOGIN_USER,
  payload: params,
});

export const saveUser = (params) => ({
  type: actionsTypes.REGISTER_USER,
  payload: params,
});

