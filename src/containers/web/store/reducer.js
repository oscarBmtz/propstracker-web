import * as actionsTypes from './types';


const INITIAL_STATE = {
  headerVisible: true,
  selectedView: '',
  registerData: {},
  userData: null,
  loadings: {
    login: false,
    register: false
  },
};


const setLoading = (state, action) => {
  const { prop, value } = action.payload;
  const loadings = { ...state.loadings };

  loadings[prop] = value;
  return { ...state, loadings };
};


// eslint-disable-next-line import/no-anonymous-default-export
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
  case actionsTypes.SET_HEADER_VISIBILITY:
    return { ...state, headerVisible: action.payload };
  case actionsTypes.SET_USER_SESSION:
    return { ...state, session: action.payload };
  case actionsTypes.SET_REGISTER_DATA:
    return { ...state, registerData: { ...state.registerData, ...action.payload } };
  case actionsTypes.SET_SELECTED_VIEW:
    return { ...state, selectedView: action.payload };
  case actionsTypes.SET_USER_DATA:
    return { ...state, userData: action.payload };
  case actionsTypes.SET_LOADING:
    return setLoading(state, action);
  default:
    return state;
  }
};
