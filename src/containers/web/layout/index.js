import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid } from "semantic-ui-react";
import { Route, Switch } from "react-router-dom";
import { isEqual } from "lodash";
import { connect } from "react-redux";
import formatter from "../../../utils/formatter";
import eventManager from "../../../utils/eventManager";
import { FadeInOutView } from "../../../components";
import { MOBILE_BREAKPOINT, SESSION_KEY } from "../../../utils/constants";
import Login from "../views/Login";
import { setMobile } from "../../app/store/actions";



const { Row, Column } = Grid;



class WebLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      footerVisible: true,
      mainFooterHeight: 0,
      scrollPosition: 0,
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
      loginModal:  false,
      registerModal: false
    };
  }

  componentDidMount() {
    this.initNavigationListeners();

    window.onresize = () => {
      this.setWindowDimensions();
      this.setFooterHeight();
    };

    this.setWindowDimensions();
    this.setFooterHeight();

    this.loginResponse = eventManager.on('login_response', (data) => {

      if(data.isValid){
        this.navigate('/dashboard')
      }
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentWillUnmount() {
    eventManager.unsubscribe("start-navigation-middle", this.middleNavigationCallbackID);
    eventManager.unsubscribe("start-navigation-finished", this.finishedNavigationCallbackID);
    eventManager.unsubscribe("navigate", this.navigateCallbackID);
    eventManager.unsubscribe("login_response", this.loginResponse);
  }

  // ---------------------
  // ----- methods -------
  // ---------------------
  setFooterHeight() {
    const mainFooter = document.getElementById("main-footer");

    if (mainFooter) {
      this.setState({ mainFooterHeight: mainFooter.offsetHeight });
    }
  }

  setWindowDimensions() {
    this.setState({
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    });

    if (window.innerWidth < MOBILE_BREAKPOINT) {
      this.props.setMobile(true);
    } else {
      this.props.setMobile(false);
    }
  }

  getMinHeight() {
    const { windowHeight, mainFooterHeight } = this.state;
    return windowHeight - mainFooterHeight;
  }

  navigate(url) {
    let eventType = "start-navigation";

    if (window.innerWidth < MOBILE_BREAKPOINT) {
      eventType = "start-navigation-mobile";
    }

    eventManager.emit(eventType, () => {
      this.props.history.push(url);
    });
  }

  initNavigationListeners() {
    this.navigateCallbackID = eventManager.on("navigate", (url) => {
      this.navigate(url);
    });

    this.middleNavigationCallbackID = eventManager.on("start-navigation-middle", () => {
      if (window.innerWidth < MOBILE_BREAKPOINT) {
        this.setState({ footerVisible: false });
      }
    });

    this.finishedNavigationCallbackID = eventManager.on("start-navigation-finished", () => {
      this.setState({ footerVisible: true });
    });
  }

  checkSession() {
    const session = window.localStorage.getItem(SESSION_KEY);

    if (session) {
      this.navigate("/dashboard");
    }
  }

  // ----------------------------
  // ----- render methods -------
  // ----------------------------
  renderMainFooter() {
    const { footerVisible } = this.state;

    if (!footerVisible) {
      return null;
    }

    return (
      <Row id="main-footer" style={{ paddingTop: 0, width: "100%", paddingBottom: 0, marginTop: 50 }}>
        <Column>
          <div style={{ background: '#E9EBEF', padding: 10 }}>
            <Grid container centered>
              <Row centered>
                <Column>
                  <p style={{ fontSize:12, color: '#7F7E89', textAlign: 'center', marginLeft: 10 }}>Copyright © 2023. Todos los derechos reservados.</p>
                </Column>
              </Row>
            </Grid>
          </div>
        </Column>
      </Row>
    );
  }

  renderViews() {
    const { language, mobile } = this.props;
    const { scrollPosition } = this.state;
    const navigate = this.navigate.bind(this);
    const commonProps = { navigate, language, mobile, scrollPosition };

    const views = (
      <Switch>
        <Route path="/" render={(props) => <Login {...props} {...commonProps} />} />
      </Switch>
    );

    return (
      <Row
        only="tablet computer"
        style={{
          minHeight: this.getMinHeight(),
          paddingBottom: 0,
          backgroundColor: "#FCF9F4",
        }}
      >
        <Column style={{ position: 'relative' }}>
          <FadeInOutView eventType="start-navigation">
            <Grid style={{ marginTop: 10, padding: 0 }}>
              <Column style={{ padding: 0 }}>{views}</Column>
            </Grid>
          </FadeInOutView>
        </Column>
      </Row>
    );
  }

  render() {

    return (
      <div>
        <Grid style={{ background: "#FCF9F4", marginTop: 0 }}>
          { this.renderViews() }
        </Grid>
      </div>
    );
  }
}


WebLayout.propTypes = {
  history: PropTypes.object,
  language: PropTypes.string,
  mobile: PropTypes.bool,
  setMobile: PropTypes.func,
  selectedView: PropTypes.string
};


const mapStateToProps = (state) => ({
  language: state.app.language,
  mobile: state.app.mobile,
  selectedView: state.web.selectedView,
});


const actions = {
  setMobile,
};


export default connect(mapStateToProps, actions)(WebLayout);
