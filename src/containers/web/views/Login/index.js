import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setSelectedView, userLogin, setUserSession } from '../../store/actions';
import MetaTags from '../../../../components/MetaTags';
import RedButton from '../../../../components/Buttons/RedButton';
import { CONTAINERS_BORDER, GRAY, RED, WHITE } from '../../../../colors';
import { Grid, Message } from 'semantic-ui-react';
import { AutoForm } from '../../../../components/AutoForm';
import { DEVICE_TOKEN, SESSION_KEY } from '../../../../utils/constants';
import tokens from '../../../../utils/tokens';
import eventManager from '../../../../utils/eventManager';
import formatter from '../../../../utils/formatter';
import { isEqual } from 'lodash';
import FadeInView from '../../../../components/FadeInView';

const { Column } = Grid;

const styles = {
  labels: {
    fontSize: 15,
    color: "#000000",
    textAlign: 'left'
    // fontWeight: "bold",
  },
  inputs: {
    color: GRAY,
    border: '1px solid rgb(200,200,200)',
    marginTop: 5
  },
  container: {
    border: CONTAINERS_BORDER,
    // border: '2px solid rgb(245,245,245)',
    backgroundColor: WHITE,
    padding: 0,
    borderRadius: 5
  }
};

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      remember_me: true,
      recoverPassword: false,
      forgotPassword: false,
      restorePassword: false,
      success: false,
      errorMessage: ''
    };
  }

  componentDidMount() {
    this.props.setSelectedView('login-view');

    this.callbackID = eventManager.on("login_response", serverResponse => {
      if (!serverResponse.isValid) {
        this.setState({ errorMessage: serverResponse.message });
      } else {
        window.localStorage.setItem(SESSION_KEY, serverResponse.data.sessionToken);

        this.props.setUserSession(serverResponse.data.sessionToken);
        this.props.navigate("/dashboard");
      }
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  getWidth() {
    if(this.props.mobile) {
      return 16;
    }

    return 8;
  }

  onLoginClick() {
    const formData = this.AutoForm.getData();

    if (formData.IsValid) {
      let deviceToken = window.localStorage.getItem(DEVICE_TOKEN);

      if (deviceToken) {
        this.props.userLogin({ Email: formData.Data.email, Password: formData.Data.password, deviceToken });
      } else {
        deviceToken = tokens.generate();
        window.localStorage.setItem(DEVICE_TOKEN, deviceToken);
        this.props.userLogin({ Email: formData.Data.email, Password: formData.Data.password, deviceToken });
      }
    }
  }

  onClickRecover() {
    const formData = this.AutoFormRecover.getData();
    if (formData.IsValid) {
      this.setState({ email: formData.Data.email, recoverPassword: true });
    }

    this.setState({ forgotPassword: false,  recoverPassword: true });
  }

  onClickVerify(){
    this.setState({ recoverPassword: false, restorePassword: true });
  }

  onClickRestorePassword() {
    this.setState({ restorePassword: false, success: true });
  }

  onClickContinue() {

  }

  getInputs(){
    return [
      {
        type: "email",
        name: "email",
        label: "Correo Electronico",
        placeholder: "Ingrese correo electronico",
        required: true,
        max: 250,
        width: 16,
        labelStyle: styles.labels,
        inputStyle: styles.inputs,
      },
      {
        type: "text",
        name: "password",
        password: true,
        label: "Contraseña",
        placeholder: "ingresar contraseña",
        required: true,
        width: 16,
        min: 8,
        max: 20,
        labelStyle: styles.labels,
        inputStyle: styles.inputs,
        onKeyUp: event => {
          if (event.key === "Enter") {
            this.onLoginClick();
          }
        }
      }
    ];
  }

  renderMessage() {

    if (this.state.errorMessage) {
      return (
        <FadeInView>
          <Message info>
            <p>{this.state.errorMessage}</p>
          </Message>
        </FadeInView>
      );
    }
  }
  renderLogin() {

    if(this.props.mobile){
      return (
        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
          <div style={{ padding: this.props.mobile ? 20 : 50 }}>
            <p style={{ color: RED, fontSize: 25, fontFamily: 'roboto-condensed-regular', fontWeight: 'bold' }} >
              Iniciar Sesión
            </p>

            <AutoForm
              showErrors
              ref={ref => {
                this.AutoForm = ref;
              }}
              inputs={this.getInputs()}
            />

            <div style={{ marginTop: 20 }}>
              {this.renderMessage()}
            </div>


            <div style={{ textAlign: 'center' }}>
              <RedButton loading={this.props.loadings.login} style={{ width: 185, height: 42, marginTop: 20 }} onClick={this.onLoginClick.bind(this)}>
                <span as='span'>Iniciar Sesión</span>
              </RedButton>
            </div>
          </div>
        </div>
      );
    }

    return (
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <div>
          <div style={{ padding: 50 }}>
            <p style={{ color: RED, fontSize: 25, fontFamily: 'roboto-condensed-regular', fontWeight: 'bold' }} >
              Iniciar Sesión
            </p>

            <AutoForm
              showErrors
              ref={ref => {
                this.AutoForm = ref;
              }}
              inputs={this.getInputs()}
            />

            <div style={{ marginTop: 20 }}>
              {this.renderMessage()}
            </div>

            <div style={{ textAlign: 'center' }}>
              <RedButton loading={this.props.loadings.login} style={{ width: 185, height: 42, marginTop: 20 }} onClick={this.onLoginClick.bind(this)}>
                <span as='span'>Iniciar Sesión</span>
              </RedButton>
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    return (
      <div style={{ position: 'relative', marginTop: 100, marginBottom: 150,  }}>
        <MetaTags
          title="Login"

        />

        <Grid centered container>
          <Column width={this.getWidth()} style={styles.container}>
            { this.renderLogin() }
          </Column>
        </Grid>
      </div>
    );
  }
}


Login.propTypes = {
  setSelectedView: PropTypes.func,
  navigate: PropTypes.func,
  mobile: PropTypes.bool,
  userLogin: PropTypes.func,
  setUserSession: PropTypes.func,
  loadings: PropTypes.object
};

const mapStateToProps = (state) => ({
  loadings: state.web.loadings
});

const actions = {
  setSelectedView,
  userLogin,
  setUserSession
};

export default connect(mapStateToProps, actions)(Login);
