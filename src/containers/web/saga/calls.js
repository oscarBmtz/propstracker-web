import axios from 'axios';
import { API_URL } from '../../../utils/constants';

export function userLogin(body) {
  return () => axios({
    method: 'post',
    url: `${API_URL}/api/users/login`,
    headers: {
      'Content-Type': 'application/json'
    },
    data: body
  });
}

export function saveUser(body) {
  return () => axios({
    method: 'post',
    url: `${API_URL}/api/users/saveUser`,
    headers: {
      'Content-Type': 'application/json'
    },
    data: body
  });
}
