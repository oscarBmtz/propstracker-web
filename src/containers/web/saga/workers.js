import { call, put } from 'redux-saga/effects';
import notifications from '../../../utils/notifications';
import eventManager from '../../../utils/eventManager';
import * as actionsTypes from '../store/types';
import * as calls from './calls';

export function* userLogin(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'login', value: true } });

    const request = calls.userLogin(action.payload);
    const response = yield call(request);
    const serverResponse = response.data;

    eventManager.emit('login_response', serverResponse);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'login', value: false } });
    yield put({ type: "app/SET_USER_DATA", payload: serverResponse.data });
  } catch (error) {
    notifications.error(typeof error === 'string' ? error : error.message);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'login', value: false } });
  }
}

export function* saveUser(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'register', value: true } });

    const currentLanguage = 'es';
    const request = calls.saveUser(action.payload, currentLanguage);
    const response = yield call(request);
    const serverResponse = response.data;

    eventManager.emit('register_response', serverResponse);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'register', value: false } });
  } catch (error) {
    notifications.error(typeof error === 'string' ? error : error.message);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'register', value: false } });
  }
}
