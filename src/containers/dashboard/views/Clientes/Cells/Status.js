import React from 'react';
import PropTypes from 'prop-types';

function Status(props) {
  const { data } = props;

  return (
    <div>
      <p style={{ marginLeft: 10, marginBottom: 0, textAlign: 'left', color: '#000000' }}>
        {data.status}
      </p>
    </div>
  );
}


Status.propTypes = {
  data: PropTypes.object
};


export default Status;
