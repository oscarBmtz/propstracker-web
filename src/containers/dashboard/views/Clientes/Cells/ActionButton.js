import React from 'react';
import PropTypes from 'prop-types';
import GrayButton from '../../../../../components/Buttons/GrayButton';

function ActionButton(props) {

  return (
    <div style={{ textAlign: 'right'}}>
      <GrayButton inverted style={{ padding: 10, marginRight: 20 }} onClick={() => props.navigate(`/dashboard/clients/${props.data.id}`)}>
      <p>Ver detalles</p>
      </GrayButton>
    </div>
  );
}


ActionButton.propTypes = {
  data: PropTypes.object,
  navigate: PropTypes.func
};


export default ActionButton;
