import React from 'react';
import PropTypes from 'prop-types';

function SecondLastName(props) {
  const { data } = props;

  return (
    <div>
      <p style={{ marginLeft: 10, marginBottom: 0, textAlign: 'left', color: '#000000' }}>
        {data.secondLastName}
      </p>
    </div>
  );
}


SecondLastName.propTypes = {
  data: PropTypes.object
};


export default SecondLastName;
