import React from 'react';
import PropTypes from 'prop-types';

function FirstLastName(props) {
  const { data } = props;

  return (
    <div>
      <p style={{ marginLeft: 10, marginBottom: 0, textAlign: 'left', color: '#000000' }}>
        {data.firstLastName}
      </p>
    </div>
  );
}


FirstLastName.propTypes = {
  data: PropTypes.object
};


export default FirstLastName;
