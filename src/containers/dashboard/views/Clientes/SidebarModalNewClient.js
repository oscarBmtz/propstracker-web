import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import SidebarModal from '../../../../components/SidebarModal';
import formatter from '../../../../utils/formatter';
import { isEqual } from 'lodash';
import { AutoForm } from '../../../../components';
import { Icon, Modal } from 'semantic-ui-react';
import { BLUE } from '../../../../colors';
import keys from '../../../../utils/keys';
import FileUpload from '../../../../components/FileUpload';
import BlueButton from '../../../../components/Buttons/BlueButton';


class sidebarModalNewClient extends Component {
  constructor(props) {
    super(props);

    this.state = {
      caseData: {},
      documents: {},
      modalExit: false
    };

  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidMount(){
    this.addNewDocument();
  }

  addNewDocument() {
    const documents = { ...this.state.documents };

    const newKey = keys.generate();
    documents[newKey] = {};

    this.setState({ documents });
  }

  getInputNewClient() {

    return [
      {
        type: 'text',
        name: 'name',
        label: 'Nombre',
        placeholder: 'Ingrese nombre',
        required: true,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'first_last_name',
        label: 'Primer Apellido',
        placeholder: 'Ingrese primer apellido',
        required: true,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'second_last_name',
        label: 'Segundo Apellido',
        placeholder: 'Ingrese segundo apellido',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'street',
        label: 'Calle',
        required: true,
        placeholder: 'Ingrese segundo apellido',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'number',
        label: 'Numero',
        required: true,
        // defaultValue: this.state.state,
        placeholder: 'Ingrese numero',
        max:13,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'neighborhood',
        label: 'Colonia',
        required: true,
        placeholder: 'Ingrese la colonia',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'zip_code',
        label: "Codigo Postal",
        // defaultValue: this.state.state,
        required: true,
        placeholder: "Ingrese codigo postal",
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'phone',
        label: "Teléfono",
        // defaultValue: this.state.state,
        required: true,
        placeholder: "Ingrese teléfono",
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'rfc',
        label: "RFC",
        required: true,
        placeholder: "Ingrese rfc",
        width: 16,
        labelStyle: { fontSize: 15 }
      }
    ];
  }

  formatObjectToArray(inputObj) {
    // Obtener las claves del objeto de entrada
    const keys = Object.keys(inputObj);

    // Mapear las claves para obtener los valores de los objetos y formatearlos en un array
    const formattedArray = keys.map(key => {
      return inputObj[key];
    });

    return formattedArray;
  }

  onClickSaveCase() {
    const { documents } = this.state;
    const autoFormData = this.AutoForm.getData();

    if (autoFormData.IsValid && Object.keys(documents).length) {
      const data = autoFormData.Data;
      this.props.onClick({
        user_id: this.props.userData.id ,
        ...data,
        phone: `${data.phone}`,
        documents: this.formatObjectToArray(documents)
      });
    }
  }

  renderCreateNewCase() {
    console.log('DOCUMENTS =>>', this.state.documents)

    return (
      <div style={{ padding: 20, position: 'relative' }}>
        <AutoForm
          showErrors
          columns={3}
          ref={(ref) => this.AutoForm = ref}
          inputs={this.getInputNewClient()}
          onChange={(formData) => this.setState({ caseData: formData  })}
        />

        <div style={{ marginTop: 30 }}>
          <div style={{ display: 'flex', position: 'absolute', right: 15, cursor: 'pointer'}} onClick={this.addNewDocument.bind(this)}>
              <p style={{ marginRight: 10 }}> Agregar documento</p>
              <Icon name='add circle' size='large' style={{ color: BLUE }}/>
          </div>

          {
            Object.keys(this.state.documents).map((key, index) =>
             <div style={{ marginBottom: 20 }}>
               <FileUpload
                key={index}
                onChange={(value) => {
                  const documents =  { ...this.state.documents };

                  if(value.isValid){
                    const aux =  {
                      file: value.file,
                      name: value.name,
                      type: "pdf"
                    }

                    documents[key] = aux

                    this.setState({ documents });
                  }
                }}
              />
             </div>
            )
          }
        </div>
      </div>
    );
  }

  render() {
    const { visible, onClose } = this.props;

    return (
      <div>
        <SidebarModal
          style={{ width: 800 }}
          visible={visible}
          onClose={() => this.setState({ modalExit: true })}
          onClickActionButton={this.onClickSaveCase.bind(this)}
          labelActionButton='Guardar'
          title='Añador Nuevo Cliente'
          loadingButton={this.props.loadings.saveClient}
        >
          <div>
            {this.renderCreateNewCase()}
          </div>

          <div>
          <Modal
            size='small'
            style={{ width: 300 }}
            open={this.state.modalExit}
            onClose={() => this.setState({ modalExit: false })}
          >
            <Modal.Content>
              Si sale toda la informacion capturada se perdera
            </Modal.Content>

            <Modal.Actions style={{ display: 'flex', justifyContent: 'space-between' }}>
              <BlueButton onClick={() => {
                this.setState({ modalExit: false });
              }}>
                Cancelar
              </BlueButton>

              <BlueButton onClick={() => {
                this.setState({ modalExit: false} , () => {
                  onClose()
                });
              }}>
                Salir
              </BlueButton>
            </Modal.Actions>
          </Modal>
          </div>
        </SidebarModal>
      </div>
    );
  }
}


sidebarModalNewClient.propTypes  = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  onClick: PropTypes.func,
  userData: PropTypes.object,
  loadings: PropTypes.object
};

const mapStateToProps = (state) => ({
  productsData: state.dashboard.productsData,
  userData: state.app.userData,
  language: state.app.language,
  loadings: state.dashboard.loadings
});


export default connect(mapStateToProps, null)(sidebarModalNewClient);
