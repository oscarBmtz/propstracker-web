import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { setSelectedView, getAllClients, saveClient } from '../../store/actions';
import BlueButton from '../../../../components/Buttons/BlueButton';
import { Grid, Modal } from 'semantic-ui-react';
import { CONTAINERS_BORDER, WHITE } from '../../../../colors';
import MetaTags from '../../../../components/MetaTags';
import CustomTable from '../../../../components/CustomTable';
import SidebarModalNewClient from './SidebarModalNewClient';
import Name from './Cells/Name';
import FirstLastName from './Cells/FirstLastName';
import SecondLastName from './Cells/SecondLastName';
import Status from './Cells/Status';
import eventManager from '../../../../utils/eventManager';
import ActionButton from './Cells/ActionButton';
import { USER_TYPES } from '../../../../utils/constants';

const { Column } = Grid;

const styles = {
  container: {
    border: CONTAINERS_BORDER,
    background: WHITE,
    borderRadius: 10,
    padding: 20
  },
  labelStyle: {
    fontSize: 15
  },
  loadingContainer: {
    height: 100,
    zIndex: 1,
    border: 'none',
    boxShadow: 'none',
    marginTop: 80
  }
};

class ClientsView extends Component {

  constructor(props){
    super(props);

      this.state = {
        sidebarModalClient: false,
        modalExit: false
      };
  }

  componentDidMount() {
    this.props.setSelectedView('clients')

    if(this.props.userData.type === USER_TYPES.ADMIN){
      this.props.getAllClients();
    } else {
      this.props.getAllClients(this.props.userData.id);
    }

    this.CallbackSaveClient = eventManager.on(('save_client'), () => {
      this.setState({ sidebarModalClient: false }, () => {
         this.props.getAllClients(this.props.userData.id);
      });
    })
  }

  componentWillUnmount() {
    eventManager.unsubscribe(this.CallbackSaveClient, 'save_client')
  }

  onClickSaveCase(data) {
    console.log('DATA TO SEND =>>>', data)
    this.props.saveClient(data);
  }

  getHeaders() {

    return [
      {
        label: "Nombre",
        component: <Name />,
        style: { background: '#E5EBEF', paddingTop: 0 }
      },
      {
        label: "Primer Apellido",
        component: <FirstLastName />,
        style: { background: '#E5EBEF', paddingTop: 0 }
      },
      {
        label: "Segundo Apellido",
        component: <SecondLastName />,
        style: { background: '#E5EBEF', paddingTop: 0 }
      },
      {
        label: "Estatus",
        component: <Status />,
        style: { background: '#E5EBEF', paddingTop: 0 }
      },
      {
        label: "",
        component: <ActionButton navigate={this.props.navigate} />,
        style: { background: '#E5EBEF', paddingTop: 0 }
      }
    ];
  }

  renderList() {
    return (
      <div>
        <div style={{ display: 'flex', justifyContent: 'space-between', alignContent: 'center', marginBottom: 30 }}>
          <p
            style={{ fontSize: 28, fontFamily: 'roboto-condensed-regular', fontWeight: 'bold', margin: 0 }}
          >
            Clientes
          </p>

         { this.props.userData.type === 'admin' ? null : <BlueButton onClick={() => this.setState({ sidebarModalClient: true })}>
            <span  style={{ fontFamily: 'Montserrat', fontSize: 15 }} >Añadir Nuevo Cliente</span>
          </BlueButton>}
        </div>

        <div style={{...styles.container }}>
          <CustomTable
            data={this.props.clientsData || []}
            headers={this.getHeaders()}
            // loading={this.props.loadings.cases}
            itemsPerPage={10}
          />
        </div>
      </div>
    );
  }

  renderContent() {
    return this.renderList();
  }


  render(){
    return (
      <div>
        <MetaTags
          title="Packaging Options"
        />

        <Grid container centered>
          <Column>
            { this.renderContent() }
          </Column>
        </Grid>

        <SidebarModalNewClient
          visible={this.state.sidebarModalClient}
          onClose={() => this.setState({ sidebarModalClient: false })}
          onClick={this.onClickSaveCase.bind(this)}
        />
      </div>
    )
  }
}

ClientsView.propTypes = {
  setSelectedView: PropTypes.func,
  userData: PropTypes.object,
  navigate: PropTypes.func,
  clientsData: PropTypes.array,
  getAllClients: PropTypes.func,
  saveClient: PropTypes.func
}

const mapStateToProps = (state) => ({
  userData: state.app.userData,
  clientsData: state.dashboard.clientsData
})

const actions = {
  setSelectedView,
  getAllClients,
  saveClient
}

export default connect(mapStateToProps, actions)(ClientsView);
