import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Button, Container, Dimmer, Form, Grid, Icon, Loader, Message, Segment, TextArea } from 'semantic-ui-react';
import { CONTAINERS_BORDER, GREEN, RED, WHITE } from '../../../../colors';
import { getClientDetail,updateClient } from '../../store/actions'
import BlueButton from '../../../../components/Buttons/BlueButton';
import RedButton from '../../../../components/Buttons/RedButton';
import { AutoForm } from '../../../../components';
import { API_URL, STATUS, USER_TYPES } from '../../../../utils/constants';
import ReusableModal from '../../../../components/ReusableModal';

const { Column } = Grid;

const styles = {
  container: {
    border: CONTAINERS_BORDER,
    background: WHITE,
    borderRadius: 10,
    padding: 20
  },
  labelStyle: {
    fontSize: 15
  },
  loadingContainer: {
    height: 100,
    zIndex: 1,
    border: 'none',
    boxShadow: 'none',
    marginTop: 80
  }
};

class ClientDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      rejectModal: false,
      authorizeModal: false
    };
  }

  componentDidMount(){
    const { id } = this.props.match.params;

    if(id){
      this.props.getClientDetail(id);
    }
  }

  getInputsClient() {
    const clientDetail = this.props.clientDetail || {};

    return [
      {
        type: 'text',
        name: 'name',
        label: 'Nombre',
        placeholder: 'Ingrese nombre',
        defaultValue: clientDetail.name,
        required: true,
        readOnly: true,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'first_last_name',
        label: 'Primer Apellido',
        placeholder: 'Ingrese primer apellido',
        defaultValue: clientDetail.firstLastName,
        required: true,
        readOnly: true,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'second_last_name',
        label: 'Segundo Apellido',
        defaultValue: clientDetail.secondLastName,
        placeholder: '',
        readOnly: true,
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'street',
        label: 'Calle',
        defaultValue: clientDetail.street,
        required: true,
        readOnly: true,
        placeholder: 'Ingrese segundo apellido',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'length',
        label: 'Numero',
        required: true,
        readOnly: true,
        defaultValue: clientDetail.number,
        placeholder: 'Ingrese numero',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'text',
        name: 'neighborhood',
        label: 'Calle',
        required: true,
        readOnly: true,
        defaultValue: clientDetail.neighborhood,
        placeholder: 'Ingrese la colonia',
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'zip_code',
        label: "Codigo Postal",
        readOnly: true,
        defaultValue: clientDetail.zipCode,
        // defaultValue: this.state.state,
        required: true,
        placeholder: "Ingrese codigo postal",
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'phone',
        label: "Teléfono",
        readOnly: true,
        defaultValue: clientDetail.phone,
        // defaultValue: this.state.state,
        required: true,
        placeholder: "Ingrese teléfono",
        width: 16,
        labelStyle: { fontSize: 15 }
      },
      {
        type: 'number',
        name: 'rfc',
        label: "RFC",
        required: true,
        defaultValue: clientDetail.rfc,
        placeholder: "Ingrese rfc",
        width: 16,
        readOnly: true,
        labelStyle: { fontSize: 15 }
      }
    ];
  }

  onClickAuthorize(clientID) {
    this.props.updateClient({ clientID, status: STATUS.AUTHORIZED });
    this.setState({ authorizeModal: false });
    this.props.navigate('/dashboard/auctions');
  }

  onClickReject(clientID) {
    this.props.updateClient({ clientID, status: STATUS.REJECTED, notes: this.state.notes });
    this.setState({ rejectModal: false });
    this.props.navigate('/dashboard/auctions');
  }

  renderStatus() {
    const { clientDetail } = this.props;

    switch (clientDetail.status) {
    case STATUS.AUTHORIZED:
      return (
        <p style={{ color: GREEN, fontWeight: 'bold', fontSize: 18 }}>Aprobado</p>
      );
    case STATUS.REJECTED:
      return (
        <p style={{ color: RED, fontWeight: 'bold', fontSize: 18 }} >Rechazado</p>
      );
    case STATUS.SEND:
      return (
        <p style={{ color: 'rgb(250, 190, 15)', fontWeight: 'bold', fontSize: 18 }}>Enviado</p>
      );
    default:
      break;
    }
  }

  renderTab() {
    const { userData, clientDetail } = this.props;
    let buttons = [];


    if (userData.type === USER_TYPES.ADMIN) {
      if (clientDetail.status === STATUS.SEND) {
        buttons = [
          {
            inverted: true,
            onClick: () => this.setState({ rejectModal: true }),
            label: "Rechazar",
            typeButton: 'red'
          },
          {
            inverted: true,
            onClick: () => this.setState({ authorizeModal: true }),
            label: "Autorizar",
            typeButton: 'blue'
          }
        ];
      }
    }

    return (
      <div style={{ ...styles.container, display: 'flex', marginBottom: 40  }}>
        <div style={{ flex: 3 }}>
          <p style={{ fontSize: 30, fontFamily: 'roboto-condensed-regular', fontWeight: 'bold', marginBottom: 10 }}>{this.props.clientDetail.name} {this.props.clientDetail.firstLastName}</p>

          { this.renderStatus() }
        </div>

        <div style={{ flex: 7, display: 'flex', alignItems: 'center', justifyContent: 'flex-end' }}>
          {
            buttons.map((btn, index) =>
              btn.typeButton === 'blue' ?
                <BlueButton key={index} inverted={btn.inverted} disabled={btn.disabled} loading={btn.loading} onClick={btn.onClick} style={{ marginRight: 10 }}>
                  {btn.label}
                </BlueButton>
                :
                btn.typeButton === 'red' ?
                  <RedButton key={index} inverted={btn.inverted} loading={btn.loading} disabled={btn.disabled} onClick={btn.onClick} style={{ marginRight: 10 }}>
                    {btn.label}
                  </RedButton>
                  : ""
            )
          }
        </div>
      </div>
    );
  }


  renderRejectedNotes() {
    const { clientDetail, userData } = this.props;

    if (clientDetail.status === STATUS.REJECTED && clientDetail.notes && userData.type !== USER_TYPES.ADMIN) {
      return (
        <div style={{ marginTop: 30, marginBottom: 30 }}>
          <Message negative >
            <Message.Header>
              Su prospecto ha sido rechazado debido a los siguiente problemas:
            </Message.Header>

            <Message.Content>
              <p style={{ color: RED, fontWeight: 'bold' }}>{clientDetail.notes}</p>
            </Message.Content>
          </Message>
        </div>
      );
    }
  }

  renderContent() {
    return (
      <div style={{ marginTop: 60 }}>
        <Button style={{ background: 'transparent', display: 'flex', alignItems: 'center', padding: 0, marginBottom: 20 }} onClick={() => this.props.navigate('/dashboard')}>
          <Icon name='chevron left' size='big' color='black' /> <span style={{ fontFamily: 'roboto-condensed-regular', fontSize: 28 }} >Todos los clientes</span>
        </Button>

        { this.renderTab() }

        { this.renderRejectedNotes() }


        <div style={{ ...styles.container }}>
          <AutoForm
            showErrors
            ref={(ref) => this.AutoForm = ref}
            columns={3}
            inputs={this.getInputsClient()}
            onChange={(formData) => this.setState({ caseData: formData })}
          />

          <div style={{ marginTop: 30 }}>
            <div style={{ marginBottom: 20 }}>
              <p style={{ fontWeight: 'bold', fontSize: 16 }}>Documentos</p>
            </div>

            <div style={{ display: 'flex' }}>
              {
                this.props.clientDetail.documents.map((mp, index) =>
                  <div key={index} style={{ display: 'flex', flexDirection: 'column', marginRight:30 }}>
                    <label style={{ marginBottom: 10, fontSize: 16 }}>
                      {mp.name}
                    </label>

                    <a href={`${API_URL}/api/assets?asset=${mp.archive}`}>
                      {mp.archive}
                    </a>
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </div>
    );
  }

  render() {
    if (this.props.loadings.clientDetail || !this.props.clientDetail) {
      return (
        <Container >
          <Segment style={styles.loadingContainer}>
            <Dimmer active inverted>
              <Loader>Loading</Loader>
            </Dimmer>
          </Segment>
        </Container>
      );
    }

    return (
      <div>
        <Grid container centered>
          <Column width={16}>
            {this.renderContent()}
          </Column>
        </Grid>

        <ReusableModal
          title="Rechazar cliente"
          actionLabel="Rechazar"
          visible={this.state.rejectModal}
          disabled={!this.state.notes}
          onClick={() => {
            this.onClickReject(this.props.clientDetail.id);
          }}
          onClose={() => { this.setState({ rejectModal: false }); }}
        >
          <p>
            ¿Esta seguro de rechazar a este cliente?
          </p>

          <Form>
            <TextArea
              style={{ marginTop: 10 }}
              placeholder="Razones"
              onChange={(e) => { this.setState({ notes: e.target.value }); }}
            />
          </Form>
        </ReusableModal>

        <ReusableModal
          title="Autorizar cliente"
          actionLabel="Autorizar"
          visible={this.state.authorizeModal}
          onClick={() => {
            this.onClickAuthorize(this.props.clientDetail.id);
          }}
          onClose={() => { this.setState({ authorizeModal: false }); }}
        >
          <p>
            ¿Esta seguro de autorizar a este cliente?
          </p>
        </ReusableModal>
      </div>
    );
  }
}

ClientDetails.propTypes = {
  navigate: PropTypes.func,
  match: PropTypes.object,
  getClientDetail: PropTypes.func,
  clientDetail: PropTypes.object,
  loadings: PropTypes.object,
  updateClient: PropTypes.func,
  userData: PropTypes.object
}

const mapStateToProps = (state) => ({
  clientDetail: state.dashboard.clientDetail,
  userData: state.app.userData,
  loadings: state.dashboard.loadings
})

const actions = {
  getClientDetail,
  updateClient,
}

export default connect(mapStateToProps, actions)(ClientDetails);
