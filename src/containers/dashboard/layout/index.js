import React, { Component } from "react";
import PropTypes from "prop-types";
import { Dimmer, Grid, Loader, Segment } from "semantic-ui-react";
import { Route, Switch } from "react-router-dom";
import { isEqual } from "lodash";
import { connect } from "react-redux";
import { setUserSession } from '../../web/store/actions';
import { getUserBySession } from '../store/actions';
import Header from "./Header";
import formatter from "../../../utils/formatter";
import eventManager from "../../../utils/eventManager";
import { FadeInOutView } from "../../../components";
import { MOBILE_BREAKPOINT, SESSION_KEY } from "../../../utils/constants";
import { setMobile } from "../../app/store/actions";
import Clients from "../../dashboard/views/Clientes";
import ClientDetails from "../../dashboard/views/ClientDetails";
import Sidebar from './Sidebar'
const { Row, Column } = Grid;


class DashboardLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      footerVisible: true,
      mainFooterHeight: 0,
      scrollPosition: 0,
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    };
  }

  componentDidMount() {
    this.initNavigationListeners();
    this.checkSession();

    window.onresize = () => {
      this.setWindowDimensions();
      this.setFooterHeight();
    };

    this.setWindowDimensions();
    this.setFooterHeight();

    window.onscroll = () => {
      const scrollPosition = window.pageYOffset;
      this.setState({ scrollPosition });
    };

    this.subscribeCallbackID = eventManager.on('subscribe:click', () => {
      const offset = this.props.mobile ? 0 : -55;

      // eslint-disable-next-line
      eval(`$(window).scrollTo('#main-footer', { duration: 1000, offset: { top: ${offset} } })`);
    });
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentWillUnmount() {
    eventManager.unsubscribe("start-navigation-middle", this.middleNavigationCallbackID);
    eventManager.unsubscribe("start-navigation-finished", this.finishedNavigationCallbackID);
    eventManager.unsubscribe("navigate", this.navigateCallbackID);
    eventManager.unsubscribe("subscribe:click", this.subscribeCallbackID);
  }

  // ---------------------
  // ----- methods -------
  // ---------------------
  setFooterHeight() {
    const mainFooter = document.getElementById("main-footer");

    if (mainFooter) {
      this.setState({ mainFooterHeight: mainFooter.offsetHeight });
    }
  }

  setWindowDimensions() {
    this.setState({
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    });

    if (window.innerWidth < MOBILE_BREAKPOINT) {
      this.props.setMobile(true);
    } else {
      this.props.setMobile(false);
    }
  }

  getMinHeight() {
    const { windowHeight, mainFooterHeight } = this.state;
    return windowHeight - mainFooterHeight;
  }

  navigate(url) {
    let eventType = "start-navigation";

    if (window.innerWidth < MOBILE_BREAKPOINT) {
      eventType = "start-navigation-mobile";
    }

    eventManager.emit(eventType, () => {
      this.props.history.push(url);
    });
  }

  initNavigationListeners() {
    this.navigateCallbackID = eventManager.on("navigate", (url) => {
      this.navigate(url);
    });

    this.middleNavigationCallbackID = eventManager.on("start-navigation-middle", () => {
      if (window.innerWidth < MOBILE_BREAKPOINT) {
        this.setState({ footerVisible: false });
      }
    });

    this.finishedNavigationCallbackID = eventManager.on("start-navigation-finished", () => {
      this.setState({ footerVisible: true });
    });
  }

  checkSession() {
    const { session } = this.props;
    const storageSession = window.localStorage.getItem(SESSION_KEY);


    if (!session && !storageSession) {
      this.navigate('/');
    }

    console.log('session', storageSession)
    if (storageSession || session) {
      this.props.setUserSession(storageSession || session);
      this.props.getUserBySession(storageSession || session);
    }
  }

  // ----------------------------
  // ----- render methods -------
  // ----------------------------
  renderFallbackView() {
    return (
      <Grid container>
        <Column>
          <Segment style={{ height: 100, marginTop: 40 }}>
            <Dimmer active inverted>
              <Loader>Loading</Loader>
            </Dimmer>
          </Segment>
        </Column>
      </Grid>
    );
  }

  renderMainFooter() {
    const { mobile } = this.props;
    const { footerVisible } = this.state;

    if (!footerVisible) {
      return null;
    }


    return (
      <Row id="main-footer" style={{ paddingTop: 0, width: "100%", paddingBottom: 0, position: 'fixed', bottom: 0, left: 0, zIndex: 100 }}>
        <Column style={{ padding: 0}}>
        <div style={{ background: "#FFFFFF", padding: '20px 0', boxShadow: '0px -3px 6px #00000029',display:'flex', justifyContent: 'center', alignItems: 'center'  }}>
            <p style={{ color: '#0D5272', fontSize: 13, marginBottom: 0, marginRight: 30 }}>
              Copyright © 2022 Catfish. Todos los derechos reservados.
            </p>
          </div>
        </Column>
      </Row>
    );
  }

  renderViews() {
    const { language, mobile, userData } = this.props;
    const { scrollPosition } = this.state;
    const navigate = this.navigate.bind(this);
    const commonProps = { navigate, language, mobile, scrollPosition };

    if (!userData) {
      return this.renderFallbackView();
    }

    const views = (
      <Switch>
         <Route path={`${this.props.match.url}/clients/:id`} render={(props) => <ClientDetails {...props} {...commonProps} />} />
         <Route path={`${this.props.match.url}/`} render={(props) => <Clients {...props} {...commonProps} />} />
      </Switch>
    );

    return (
      <Row
        only="tablet computer"
        style={{
          minHeight: this.getMinHeight(),
          paddingBottom: 0,
          backgroundColor: "#FCF9F4",
        }}
      >
        <Column style={{ marginTop: 30 }}>
          <FadeInOutView eventType="start-navigation">
            <Grid style={{ marginTop: 60, padding: 0 }}>
              <Column style={{ padding: 0 }}>{views}</Column>
            </Grid>
          </FadeInOutView>
        </Column>
      </Row>
    );
  }

  render() {
    const { mobile } = this.props;
    const { scrollPosition } = this.state;
    const extraStyle = {};

    if (mobile) {
      extraStyle.width = '100%';
      extraStyle.marginLeft = 0;
    }

    return (
      <div>
        <Header
          navigate={this.navigate.bind(this)}
          onClickLeftButton={() => {
            this.setState({ visibleSidebar: !this.state.visibleSidebar });
          }}
        />

        <Sidebar
          navigate={this.navigate.bind(this)}
          mobile={this.props.mobile}
          onClickLeftButton={() => {
            this.setState({ visibleSidebar: !this.state.visibleSidebar });
          }}
        />

        <Grid style={{ background: 'rgb(250,250,250)', margin: 0, marginLeft: 250 }}>
          { this.renderViews() }
        </Grid>
      </div>
    );
  }
}


DashboardLayout.propTypes = {
  history: PropTypes.object,
  language: PropTypes.string,
  mobile: PropTypes.bool,
  setMobile: PropTypes.func,
  selectedView: PropTypes.string,
  match: PropTypes.object,
  userData: PropTypes.object,
  setUserSession: PropTypes.func,
  getUserBySession: PropTypes.func,
  session: PropTypes.string
};


const mapStateToProps = (state) => ({
  language: state.app.language,
  mobile: state.app.mobile,
  selectedView: state.web.selectedView,
  userData: state.app.userData,
  session: state.web.session
});


const actions = {
  setMobile,
  getUserBySession,
  setUserSession
};


export default connect(mapStateToProps, actions)(DashboardLayout);
