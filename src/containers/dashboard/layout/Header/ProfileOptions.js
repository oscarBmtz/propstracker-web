import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Icon, List, Popup } from 'semantic-ui-react';
import { BLUE, DARK_GRAY, GREEN, RED, WHITE, YELLOW } from '../../../../colors';
import { STATUS } from '../../../../utils/constants';
import formatter from '../../../../utils/formatter';


class ProfileOptions extends Component {
  onClickLogOut() {
    // getWindow().localStorage.removeItem(MEAT_SESSION_KEY);
    this.props.navigate('/');
  }

  render() {
    const { userData } = this.props;

    if (!userData) {
      return null;
    }

    const firstName = formatter.capitalize(userData.email || '');
    const firstLetters = `${firstName && firstName.substring(0, 1)}`;

    let reviewIcon;

    if (userData.status === STATUS.REVIEW) {
      reviewIcon = <Icon name='time' style={{ color: '#EBA835', marginLeft: 10 }} />;
    }

    return (
      <div>
        <Popup
          position="top right"
          wide
          on='click'
          style={{ padding: 0 }}
          trigger={
            <div style={{ display: 'flex', alignItems: 'center', marginLeft: 10, borderLeft: `1px solid #DEDEDF`, cursor: 'pointer', marginRight: 15 }}>
              <div style={{
                display: 'flex', justifyContent: 'center', alignItems: 'center', marginLeft: 10, width: 45, height: 45, borderRadius: '50%', background: BLUE, color: WHITE,
                border: `2px solid #DEDEDF`
              }}>
                <span style={{ fontWeight: 'bold' }}>{firstLetters}</span>
              </div>

              <div style={{ marginLeft: 5 }}>
                <Icon name="chevron down" style={{ color: '#323232' }} />
              </div>
            </div>
          }
        >
          <Popup.Content>
            <div>
              <List selection style={{ background: '#F8F9FA', marginBottom: 10, padding: 15 }}>
                <div style={{ paddingLeft: 7 }}>
                  <p style={{ fontFamily: 'roboto-condensed-regular', fontSize: 24, color: '#1A1A1A', marginBottom: 5 }}>{userData.name} {userData.firstLastName} </p>
                  <p style={{ color: DARK_GRAY, fontSize: 15, marginBottom: 5 }}>{userData.email}</p>
                </div>
              </List>

              <List selection style={{ padding: 15, margin: 0 }}>

                <List.Item>
                  <List.Content>
                    <p
                      style={{ color: RED, cursor: 'pointer' }}
                      onClick={this.onClickLogOut.bind(this)}
                    >
                      Cerrar Sesion
                    </p>
                  </List.Content>
                </List.Item>
              </List>
            </div>
          </Popup.Content>
        </Popup>
      </div>
    );
  }
}


ProfileOptions.propTypes = {
  userData: PropTypes.object,
  navigate: PropTypes.func
};


const mapStateToProps = (state) => ({
  userData: state.app.userData
});


export default connect(mapStateToProps, null)(ProfileOptions);
