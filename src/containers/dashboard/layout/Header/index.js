import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { isEqual } from "lodash";
import { Grid } from "semantic-ui-react";
import formatter from "../../../../utils/formatter";
import { WHITE, BLUE } from "../../../../colors";
import OPTIONS from "./options.json";
import ProfileOptions from "./ProfileOptions";

const { Row, Column } = Grid;

const styles = {
  headerContainer: {
    background: WHITE,
    boxShadow: 'rgba(0, 0, 0, 0.2) 0px 1px 3px',
    zIndex: 200,
    position: 'fixed',
    top: 0,
    left: 0,
    margin: 0,
    width: '100%'
  },
};


class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hoverOption: null,
      mobileMenuVisible: false,
      loginModalVisible: false,
      changeLanguaje: false,
      registerOptionsVisible: false
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  // -------------------
  // ----- methods -----
  // --------------------

  onMainLogoClick() {
    this.setState({ mobileMenuVisible: false });
    this.props.navigate("/dashboard");
  }

  onOptionClick(option) {
    if (option.url !== undefined) {
      this.props.navigate(option.url);
    }
  }


  getHeaderClass() {
    const { headerVisible } = this.props;
    return headerVisible ? "visible-header" : "hidden-header";
  }


  renderDesktopOptions() {
    const { hoverOption } = this.state;

    return OPTIONS.map((option) => {
      let buttonStyle = styles.menuButton;

      if (hoverOption === option.id) {
        buttonStyle = { ...buttonStyle, ...styles.hoveredMenuButton };
      }

      if (this.props.selectedView === option.id) {
        buttonStyle = { ...buttonStyle, color: BLUE };
      }

      return (
        <a
          key={option.id}
          href={option.url}
          onMouseEnter={() => this.setState({ hoverOption: option.id })}
          onMouseLeave={() => this.setState({ hoverOption: null })}
          onClick={(event) => {
            event.preventDefault();
            this.onOptionClick(option);
          }}
        >
          <span style={buttonStyle}>{option.label}</span>
        </a>
      );
    });
  }


  renderDesktopHeader() {

    return (
      <div style={{ display: 'flex', alignItems: 'center', height: 60 }}>
        <div style={{ flex: 4,  display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
          <p style={{ padding: 10, margin: 0, marginLeft: 10, fontSize: 26, fontWeight: 'bold'}}>Props Tracker</p>

          <div style={{ display: 'flex', alignItems: 'center' }}>
            <ProfileOptions navigate={this.props.navigate}/>
          </div>
        </div>
      </div>
    );
  }

  // --------------------------
  // ----- render methods -----
  // --------------------------

  render() {
    const { mobile, userData } = this.props;
    const mainHeaderStyle = { ...styles.headerContainer };

    if (mobile) {
      mainHeaderStyle.boxShadow = '0px 2px 2px rgba(0,0,0,0.2)';
    }

    if(!userData){
      return null;
    }

    let content = this.renderDesktopHeader();

    if(this.props.mobile){
      content = this.renderMobileHeader();
    }

    return (
      <Grid id="main-header" className={this.getHeaderClass()} style={mainHeaderStyle}>
      <Row style={{ padding: 0 }}>
        <Column style={{ padding: 0 }}>
          {content}
        </Column>
      </Row>
    </Grid>
    );
  }
}


Header.propTypes = {
  navigate: PropTypes.func,
  headerVisible: PropTypes.bool,
  selectedView: PropTypes.string,
  userData: PropTypes.object,
  //language: PropTypes.string,
  mobile: PropTypes.bool
};


const mapStateToProps = (state) => ({
  headerVisible: state.web.headerVisible,
  language: state.app.language,
  selectedView: state.web.selectedView,
  userData: state.app.userData
});


export default connect(mapStateToProps)(Header);
