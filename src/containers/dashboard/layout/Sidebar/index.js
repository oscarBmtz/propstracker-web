import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { isEqual } from 'lodash';
import { Grid, Icon, List } from 'semantic-ui-react';
import { SESSION_KEY } from '../../../../utils/constants';
import formatter from '../../../../utils/formatter';
import { DARK_GRAY, GRAY, RED, WHITE } from '../../../../colors';
import eventManager from '../../../../utils/eventManager';
import i18n from '../i18n.json';

const styles = {
  icon: {
    marginLeft: 10,
    marginRight: 10,
  },
  sideMenu: {
    background: WHITE,
    boxShadow: '0 3px 3px rgb(0,0,0,0.1)',
    zIndex: 190,
    position: 'fixed',
    top: 0,
    left: 0,
    width: 250,
    padding: 0,
    height: '100%',
    overflowY: 'auto'
  },
  option: {
    color: GRAY,
    display: 'flex',
    alignItems: 'center',
    height: 40,
    transition: 'all 200ms ease 0s',
    cursor: 'pointer',
  },
};

const { Column } = Grid;


class Sidebar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      optionHovered: null,
      selectedOption: '',
      subOptionSelected: null,
      collapseProfile: false,
      favorite_tag: '',
    };
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  navigate(url) {
    this.props.navigate(url);
  }

  onSignOff() {
    window.localStorage.removeItem(SESSION_KEY);
    this.props.navigate('/');
  }

  renderProfileOptions() {
    return (
      <div>
        <List selection verticalAlign='middle' style={{ fontSize: 15 }}>
          <List.Item>
            <List.Content>
              <div onClick={this.onSignOff.bind(this)}>
                <Icon name="sign-out" />
                <span>Cerrar Session</span>
              </div>
            </List.Content>
          </List.Item>
        </List>
      </div>
    );
  }

  renderOptions() {
    const { selectedView, userData } = this.props;

    return i18n.options.filter((fd) => fd.role.includes(userData.type)).map((option) => {
      const extraStyle = {};

      if (selectedView === option.id) {
        extraStyle.borderLeft = `5px solid ${RED}`;
        extraStyle.background = 'rgb(235 235 237)';
        extraStyle.fontWeight= 'bold';
      }

      if (this.state.hoverOption && this.state.hoverOption.id === option.id) {
        extraStyle.background = 'rgb(235 235 237)';
      }



      console.log('option', option)
      return (
        <div
          onClick={(event) => {
            if (this.props.onToogleSidebar) {
              this.props.onToogleSidebar();
            }

            event.preventDefault();

            if (option.id === 'signOff') {
              eventManager.emit('navigate', '');
            } else {
              eventManager.emit('navigate', option.url);
              console.log('oirlption', option.url)

            }
          }}
          onMouseEnter={() => this.setState({ hoverOption: option })}
          onMouseLeave={() => this.setState({ hoverOption: null })}
          key={option.id} style={{ fontSize: 16, color: WHITE, padding: '30px 0px', ...styles.option, ...extraStyle }}
        >
          <a
            style={{ textDecoration: 'none', userSelect: 'none' , marginLeft: 20, color: GRAY }}
            key={option.id}
            href={option.url}
          >
            { option.label[this.props.language] }
          </a>
        </div>
      );
    }
    );
  }

  render() {
    const { collapseProfile } = this.state;
    const collapseIcon = collapseProfile ? 'angle up' : 'angle down';

    if (!this.props.userData) {
      return null;
    }

    if (this.props.mobile) {
      return (
        <div>
          <div style={{ paddingTop: 10, paddingBottom: 10, paddingLeft: 8, background: '#f5f5f5', }}>
            <div onClick={() => this.setState({ collapseProfile: !collapseProfile })} style={{ width: '50%' }}>
              <p style={{ margin: 0 }}>Jhon Doe <Icon name={collapseIcon} style={{ color: '#000', ...styles.icon }} size="large" /></p>
            </div>

            { collapseProfile ? this.renderProfileOptions() : null }
          </div>

          { this.renderOptions() }
        </div>
      );
    }

    const customStyle = {
      ...styles.sideMenu,
      marginTop: 60,
      height: '100%'
    };

    return (
      <div style={{ ...customStyle }} id='main-sidebar'>
        <Grid style={{ top: 0, left: 0, margin: 0 }}>
          <Column style={{ paddingLeft: 0, paddingRight: 0, paddingTop: 0 }}>
            <div style={{ padding: 0 }}>
              <div>
                { this.renderOptions() }
              </div>
            </div>
          </Column>
        </Grid>
      </div>
    );
  }
}


Sidebar.propTypes = {
  navigate: PropTypes.func,
  language: PropTypes.string,
  onToogleSidebar: PropTypes.func,
  userData: PropTypes.object,
  selectedView: PropTypes.string,
  mobile: PropTypes.bool
};


const mapStateToProps = (state) => ({
  language: state.app.language,
  favorites: state.dashboard.favorites,
  alerts: state.dashboard.alerts,
  userData: state.app.userData,
  selectedView: state.dashboard.selectedView,
  selectedItem: state.dashboard.selectedSubMenuItem
});


export default connect(mapStateToProps)(Sidebar);
