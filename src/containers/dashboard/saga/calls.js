import axios from 'axios';
import { API_URL } from '../../../utils/constants';

export function getUserBySession(session) {
  return () => axios({
    method: 'get',
    url: `${API_URL}/api/users/by-session`,
    headers: {
      'Content-Type': 'application/json',
      'Authorization': session
    },
  });
}

export function getAllClients(userID) {
  return () => axios({
    method: 'get',
    url: `${API_URL}/api/clients/get-all/${userID || ''}`,
    headers: {
      'Content-Type': 'application/json'
    }
  });
}

export function saveClient(body) {
  return () => axios({
    method: 'post',
    url: `${API_URL}/api/clients/save`,
    headers: {
      'Content-Type': 'application/json'
    },
    data: body
  });
}

export function getClientDetail(clientID) {
  return () => axios({
    method: 'get',
    url: `${API_URL}/api/clients/get/${clientID}`,
    headers: {
      'Content-Type': 'application/json'
    }
  });
}

export function updateClient(body) {
  return () => axios({
    method: 'put',
    url: `${API_URL}/api/clients/updated`,
    headers: {
      'Content-Type': 'application/json'
    },
    data: body
  });
}
