import { call, put } from 'redux-saga/effects';
import notifications from '../../../utils/notifications';
// import eventManager from '../../../utils/eventManager';
import * as actionsTypes from '../store/types';
import * as calls from './calls';
import eventManager from '../../../utils/eventManager';
import { SESSION_KEY } from '../../../utils/constants';

export function* getUserBySession(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'userData', value: true } });

    const request = calls.getUserBySession(action.payload);
    const response = yield call(request);
    const serverResponse = response.data;

    if (serverResponse.isValid && serverResponse.data) {
      yield put({ type: 'app/SET_USER_DATA', payload: serverResponse.data });
    } else {
      window.localStorage.removeItem(SESSION_KEY);
      eventManager.emit('navigate', '/');
      // throw new Error(serverResponse.message);
    }

    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'userData', value: false } });
  } catch (error) {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'userData', value: false } });
    notifications.error(typeof error === 'string' ? error : error.message);
  }
}

export function* getAllClients(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientsData', value: true } });

    const request = calls.getAllClients(action.payload);
    const response = yield call(request);
    const serverResponse = response.data

    if(serverResponse.isValid) {
      yield put({ type: actionsTypes.SET_ALL_CLIENTS, payload: serverResponse.data });
    }

    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientsData', value: false } });
  } catch (error) {
    notifications.error(typeof error === 'string' ? error : error.message);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientsData', value: false } });
  }
}

export function* saveClient(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'saveClient', value: true } });

    const request = calls.saveClient(action.payload);
    const response = yield call(request);
    const serverResponse = response.data

    if(serverResponse.isValid) {
      eventManager.emit('save_client')
    }

    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'saveClient', value: false } });
  } catch (error) {
    notifications.error(typeof error === 'string' ? error : error.message);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'saveClient', value: false } });
  }
}

export function* getClientDetail(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientDetail', value: true } });

    const request = calls.getClientDetail(action.payload);
    const response = yield call(request);
    const serverResponse = response.data

    if(serverResponse.isValid) {
      yield put({ type: actionsTypes.SET_CLIENT_DETAIL, payload: serverResponse.data });
    }

    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientDetail', value: false } });
  } catch (error) {
    notifications.error(typeof error === 'string' ? error : error.message);
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'clientDetail', value: false } });
  }
}

export function* updateClient(action) {
  try {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'updateClient', value: true } });

    const request = calls.updateClient(action.payload);
    const response = yield call(request);
    const serverResponse = response.data;

    if (serverResponse.isValid) {
      eventManager.emit('refresh-clients');
    } else {
      notifications.error(serverResponse.message);
    }

    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'updateClient', value: false } });
  } catch (error) {
    yield put({ type: actionsTypes.SET_LOADING, payload: { prop: 'updateClient', value: false } });
    notifications.error(typeof error === 'string' ? error : error.message);
  }
}

