import { takeLatest, all } from 'redux-saga/effects';
import * as actionsTypes from '../store/types';
import * as workers from './workers';


export default function* watcherSaga() {
  yield all([
    takeLatest(actionsTypes.GET_USER_BY_SESSION, workers.getUserBySession),
    takeLatest(actionsTypes.GET_ALL_CLIENTS, workers.getAllClients),
    takeLatest(actionsTypes.SAVE_CLIENT, workers.saveClient),
    takeLatest(actionsTypes.GET_CLIENT_DETAIL, workers.getClientDetail),
    takeLatest(actionsTypes.UPDATE_CLIENT, workers.updateClient),
  ]);
}
