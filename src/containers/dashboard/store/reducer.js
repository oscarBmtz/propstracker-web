import * as actionsTypes from './types';


const INITIAL_STATE = {
  headerVisible: true,
  selectedView: '',
  session: null,
  postDetail: null,
  clientsData: null,
  clientDetail: null,
  loadings: {
    clientDetail: false,
    clientsData: false
  },
};


const setLoading = (state, action) => {
  const { prop, value } = action.payload;
  const loadings = { ...state.loadings };

  loadings[prop] = value;
  return { ...state, loadings };
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionsTypes.SET_HEADER_VISIBILITY:
      return { ...state, headerVisible: action.payload };
    case actionsTypes.SET_USER_SESSION:
      return { ...state, session: action.payload };
    case actionsTypes.SET_SELECTED_VIEW:
      return { ...state, selectedView: action.payload };
    case actionsTypes.SET_ALL_CLIENTS:
      return { ...state, clientsData: action.payload };
    case actionsTypes.SET_CLIENT_DETAIL:
      return { ...state, clientDetail: action.payload };
    case actionsTypes.SET_LOADING:
      return setLoading(state, action);
    default:
      return state;
  }
};
