import * as actionsTypes from './types';

export const setHeaderVisibility = (params) => ({
  type: actionsTypes.SET_HEADER_VISIBILITY,
  payload: params,
});

export const setUserSession = (params) => ({
  type: actionsTypes.SET_USER_SESSION,
  payload: params,
});

export const getUserBySession = (params) => ({
  type: actionsTypes.GET_USER_BY_SESSION,
  payload: params
});

export const setSelectedView = (params) => ({
  type: actionsTypes.SET_SELECTED_VIEW,
  payload: params,
});

export const getAllClients = (params) => ({
  type: actionsTypes.GET_ALL_CLIENTS,
  payload: params,
});

export const saveClient = (params) => ({
  type: actionsTypes.SAVE_CLIENT,
  payload: params,
});

export const getClientDetail = (params) => ({
  type: actionsTypes.GET_CLIENT_DETAIL,
  payload: params,
});

export const updateClient = (params) => ({
  type: actionsTypes.UPDATE_CLIENT,
  payload: params,
});
