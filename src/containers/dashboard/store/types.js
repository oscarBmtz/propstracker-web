export const SET_HEADER_VISIBILITY = 'dashboard/SET_HEADER_VISIBILITY';
export const SET_LOADING = 'dashboard/SET_LOADING';
export const SET_USER_SESSION = 'dashboard/SET_USER_SESSION';
export const GET_USER_BY_SESSION = 'dashboard/GET_USER_BY_SESSION';
export const SET_SELECTED_VIEW = 'dashboard/SET_SELECTED_VIEW';

export const GET_ALL_CLIENTS = 'dashboard/GET_ALL_CLIENTS';
export const SET_ALL_CLIENTS = 'dashboard/SET_ALL_CLIENTS';
export const SAVE_CLIENT = 'dashboard/SAVE_CLIENT';


export const GET_CLIENT_DETAIL = 'dashboard/GET_CLIENT_DETAIL';
export const SET_CLIENT_DETAIL = 'dashboard/SET_CLIENT_DETAIL';

export const UPDATE_CLIENT = 'dashboard/UPDATE_CLIENT';

