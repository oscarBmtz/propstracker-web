import * as actionsTypes from './types';
import { MOBILE_BREAKPOINT } from '../../../utils/constants';


const INITIAL_STATE = {
  language: 'en',
  mobile: window.innerWidth < MOBILE_BREAKPOINT,
  userData: null
};

// eslint-disable-next-line import/no-anonymous-default-export
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionsTypes.SET_LANGUAGE:
      return { ...state, language: action.payload };
    case actionsTypes.SET_MOBILE:
        return { ...state, mobile: action.payload };
    case actionsTypes.SET_USER_DATA:
      return { ...state, userData: action.payload };
    default:
      return state;
  }
};
