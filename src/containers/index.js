import { combineReducers } from 'redux';
import appContainer from './app';
import webContainer from './web';
import dashboardContainer from './dashboard'


const containers = {
  reducers: combineReducers({
    app: appContainer.reducer,
    dashboard: dashboardContainer.reducer,
    web: webContainer.reducer,
  }),
  sagas: {
    dashboard: dashboardContainer.saga,
    web: webContainer.saga,
  },
};


export default containers;
