import React, { Component } from "react";
import { Provider } from "react-redux";
import configureStore from "../configureStore";
import eventManager from "../utils/eventManager";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import WebLayout from "../containers/web/layout";
import DashboardLayout from "../containers/dashboard/layout";
import { SHOW_NOTIFICATION } from "../utils/constants";

const store = configureStore();

class App extends Component {
  componentDidMount() {
    // this.callbackID = eventManager.on(SHOW_NOTIFICATION, (data) => {
    //   if (this.notificationSystem) {
    //     this.notificationSystem.addNotification({
    //       level: data.type,
    //       message: data.message,
    //     });
    //   }
    // });
  }

  componentWillUnmount() {
    eventManager.unsubscribe(SHOW_NOTIFICATION, this.callbackID);
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Switch>
            <Route path="/dashboard" render={(props) => <DashboardLayout {...props} />} />
            <Route path="/" render={(props) => <WebLayout {...props} />} />
          </Switch>
        </Router>
      </Provider>
    );
  }
}

export default App;
