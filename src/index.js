import React from "react";
import App from "./app";
import { createRoot } from 'react-dom/client';
import "./index.css";
import "react-responsive-carousel/lib/styles/carousel.min.css";

const container = document.getElementById('root');
const root = createRoot(container); // createRoot(container!) if you use TypeScript
root.render(<App />);

// // If you want your app to work offline and load faster, you can change
// // unregister() to register() below. Note this comes with some pitfalls.
// // Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
