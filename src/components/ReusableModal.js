import React, { Component } from "react";
import PropTypes from "prop-types";
import { Grid, Divider, Button } from "semantic-ui-react";
import { BLUE, RED, WHITE } from "../colors";
import AnimatedModal from './AnimatedModal';
import formatter from "../utils/formatter";
import { isEqual } from "lodash";

const { Row, Column } = Grid;


class ReusableModal extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  closeModal() {
    this.props.onClose();
  }

  renderAlterButton() {
    const { alterLabel, readOnlyAlter } = this.props;
    if (readOnlyAlter) {
      return <span style={{ marginRight: 5 }}>{alterLabel}</span>;
    }

    return (
      <Button
        onClick={this.closeModal.bind(this)}
        style={{ backgroundColor: WHITE, color: BLUE }}>
        {alterLabel ? alterLabel : 'Cancel'}
      </Button>
    );
  }

  renderContent() {
    const { mobile, title, actionLabel, children, readOnly, noSeparator } = this.props;
    return (
      <Grid centered>
        <Row centered style={{ paddingTop: 10, paddingBottom: 0 }}>
          <Column
            width={16}
            textAlign='center'
            style={{
              color: 'black',
              fontFamily: 'bebas-neue',
              fontSize: mobile ? 30 : 25
            }}
          >
            <span style={{ color: RED }}>{title.charAt(0)}</span>{title.substr(1)}
          </Column>
        </Row>
        {noSeparator ? null : <Divider />}
        <Row centered>
          <Column width={16}>
            {children}
          </Column>
        </Row>

        {readOnly ? null : (<Row centered>
          <Column width={16}>
            <div style={{ width: '100%', textAlign: 'right' }}>
              {this.renderAlterButton()}
              <Button
                loading={this.props.loading}
                disabled={this.props.disabled}
                onClick={() => { this.props.onClick(); }}
                style={{ backgroundColor: BLUE, color: WHITE, borderRadius: 5 }}>
                {actionLabel}
              </Button>
            </div>
          </Column>
        </Row>)}
      </Grid>
    );
  }

  render() {
    const { visible, style } = this.props;

    return (
      <AnimatedModal
        className="hide-scroll"
        size="large"
        onClose={this.closeModal.bind(this)}
        open={visible}
        style={{
          width: 400,
          ...style,
        }}
      >
        {this.renderContent()}
      </AnimatedModal>
    );
  }
}


ReusableModal.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  mobile: PropTypes.bool,
  title: PropTypes.string,
  actionLabel: PropTypes.string,
  alterLabel: PropTypes.string,
  children: PropTypes.any,
  style: PropTypes.object,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  readOnlyAlter: PropTypes.bool,
  readOnly: PropTypes.bool,
  noSeparator: PropTypes.bool
};


export default ReusableModal;
