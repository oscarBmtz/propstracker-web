import React, { Component } from "react";
import { Icon } from "semantic-ui-react";
import PropTypes from "prop-types";
import { isEqual } from "lodash";
import formatter from "../utils/formatter";
//import { ORANGE, WHITE } from "../colors";
import { BLACK } from "../colors";

class ExpansibleContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      contentVisible: false,
      contentHeight: 0,
    };
  }

  contentRef = React.createRef();

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return (
      !isEqual(nextState, this.state) ||
      !isEqual(clean(nextProps), clean(this.props))
    );
  }

  componentDidUpdate(prevProps, prevState) {
    if (!prevState.contentVisible && this.state.contentVisible) {
      this.calcHeight();
    }

    if (prevState.contentVisible && !this.state.contentVisible) {
      this.setState({ contentHeight: 0 });
    }
  }

  toogleTransition() {
    this.setState({ contentVisible: !this.state.contentVisible });
  }

  calcHeight() {
    const contentHeight = this.contentRef.current?.firstChild.offsetHeight;
    this.setState({ contentHeight });
  }

  render() {
    const { contentHeight, contentVisible } = this.state;
    const { style, label, children } = this.props;

    let boxShadow;

    const { mobile } = this.props;

    let contentStyle = {
      transition: "all 500ms ease",
      height: contentHeight,
      fontSize: 17,
      padding: !mobile ? '0px 56px' : '0px 0px',
      marginTop: 10
      // overflow: "hidden",
    };

    if (contentVisible) {
      contentStyle.marginBottom = 40;
      boxShadow = "0 2px 7px rgb(0,0,0,0.15)";
    }

    return (
      <div style={{
        ...style, overflow: "hidden", cursor: "pointer",
        display: "flex",
        justifyContent: "space-between",
        flexDirection: "column",
        marginBottom: 10,
        //boxShadow
      }}>
        <div
          style={{
            display: "flex",
            color: contentVisible && BLACK,
            fontWeight: contentVisible && 'bold',
            background: contentVisible && '#F9F9F9',
            border: "1px solid #DAE5EA",
            borderRadius: 10
          }}
          onClick={this.toogleTransition.bind(this)}
        >
          <div style={{ padding: 10 }}>
            <Icon size='big' name={contentVisible ? "angle down" : "angle right"} />
          </div>

          <div style={{ display: 'flex', alignItems: 'center', fontSize: 17 }}>
            {label}
          </div>
        </div>

        <div ref={this.contentRef} style={contentStyle}>
          <div>{children}</div>
        </div>
      </div>
    );
  }
}

ExpansibleContainer.propTypes = {
  style: PropTypes.object,
  mobile: PropTypes.bool,
  label: PropTypes.element,
  children: PropTypes.oneOfType([PropTypes.element, PropTypes.array]),
};

export { ExpansibleContainer };
