import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { WHITE, GREEN, DARK_GREEN } from '../../colors';


class GreenButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  render() {
    const { hovering } = this.state;
    const { children, loading, fluid, style = {}, disabled, inverted } = this.props;

    const buttonStyle = {
      background: inverted ? 'transparent' : GREEN,
      border: `1px solid ${GREEN}`,
      borderBottom: inverted ? `3px solid ${GREEN}` : `3px solid ${DARK_GREEN}`,
      color: inverted ? DARK_GREEN: WHITE,
      borderRadius: 5,
      fontFamily: 'Montserrat',
      ...style
    };

    if (hovering) {
      buttonStyle.background = inverted ? GREEN : DARK_GREEN;
      buttonStyle.color = WHITE;
    }

    return (
      <Button
        loading={loading}
        fluid={fluid}
        style={buttonStyle}
        onClick={this.onClick.bind(this)}
        disabled={disabled}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      >
        { children }
      </Button>
    );
  }
}


GreenButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool
};


export default GreenButton;
