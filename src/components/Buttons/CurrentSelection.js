import React, { Component } from "react";
import PropTypes from 'prop-types';
import { Button, Popup } from 'semantic-ui-react';
import { GRAY, WHITE } from "../../colors";

const styles = {
  button: {
    marginRight: 10,
    background: 'transparent',
    borderRadius: 7,
    padding: '5px 18px',
    fontSize: 12,
    fontWeight: 'bold',
    boxShadow: `0px 2px 4px rgb(0, 0, 0, 0.15)`
  },
  disabled: {
    border: `1px solid ${WHITE}`,
    color: WHITE,
    background: GRAY
  }
};

class CurrentSelection extends Component {
  constructor(props) {
    super(props);
    this.state = {
      hovering: false,
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  renderButton() {
    const { label, disabled, style, hoveringStyle, loading } = this.props;
    const { hovering } = this.state;
    let finalStyle;

    if (hovering) {
      finalStyle = { ...styles.button, ...hoveringStyle };
    } else {
      finalStyle = { ...styles.button, ...style };
    }

    return (
      <Button
        loading={loading}
        disabled={disabled}
        className='redButton'
        style={finalStyle}
        content={label}
        onClick={this.onClick.bind(this)}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      />
    );
  }

  render() {
    const { style, popupLabel } = this.props;

    if (popupLabel) {
      return (
        <Popup
          content={popupLabel}
          style={{ style, borderRadius: 5 }}
          trigger={this.renderButton()}
        />
      );
    }

    return this.renderButton();
  }
}

CurrentSelection.propTypes = {
  label: PropTypes.string,
  onClick: PropTypes.func,
  style: PropTypes.object,
  hoveringStyle: PropTypes.object,
  popupLabel: PropTypes.string,
  disabled: PropTypes.bool,
  loading: PropTypes.bool
};

export default CurrentSelection;
