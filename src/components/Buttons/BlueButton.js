import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { BLUE, DARK_BLUE, WHITE } from '../../colors';


class BlueButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  render() {
    const { hovering } = this.state;
    const { children, loading, fluid, style = {}, disabled, inverted } = this.props;

    const buttonStyle = {
      background: inverted ? 'transparent' : BLUE,
      border: `1px solid ${BLUE}`,
      borderBottom: inverted ? `3px solid ${BLUE}` : `3px solid ${DARK_BLUE}`,
      color: inverted ? DARK_BLUE: WHITE,
      borderRadius: 5,
      fontFamily: 'Montserrat',
      ...style
    };

    if (hovering) {
      buttonStyle.background = inverted ? BLUE : DARK_BLUE;
      buttonStyle.color = WHITE;
    }

    return (
      <Button
        loading={loading}
        fluid={fluid}
        style={buttonStyle}
        onClick={this.onClick.bind(this)}
        disabled={disabled}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      >
        { children }
      </Button>
    );
  }
}


BlueButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool
};


export default BlueButton;
