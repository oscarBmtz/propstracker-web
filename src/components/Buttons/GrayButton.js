import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { DARK_GRAY, WHITE } from '../../colors';


class GrayButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  render() {
    const { hovering } = this.state;
    const { children, loading, fluid, style = {}, disabled, inverted } = this.props;

    const buttonStyle = {
      background: inverted ? 'transparent' : DARK_GRAY,
      border: `1px solid ${DARK_GRAY}`,
      borderBottom: inverted ? `3px solid ${DARK_GRAY}` : `3px solid #313131`,
      color: inverted ? DARK_GRAY: WHITE,
      borderRadius: 5,
      fontFamily: 'Montserrat',
      ...style
    };

    if (hovering) {
      buttonStyle.background = inverted ? DARK_GRAY : '#313131';
      buttonStyle.color = WHITE;
    }

    return (
      <Button
        loading={loading}
        fluid={fluid}
        style={buttonStyle}
        onClick={this.onClick.bind(this)}
        disabled={disabled}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      >
        { children }
      </Button>
    );
  }
}


GrayButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool
};


export default GrayButton;
