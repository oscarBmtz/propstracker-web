import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { RED, WHITE } from '../../colors';
import { Button } from 'semantic-ui-react';


class HoverableButton extends Component {
  constructor (props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  render () {
    const { hovering } = this.state;
    const { background, children, onClick, fluid, style = {}, loading, href } = this.props;
    let backgroundColor;
    let border;

    if (background === 'blue') {
      backgroundColor = '#7798BF';
      border = '1px solid #7798BF';
    } else {
      backgroundColor = '#bf0a30';
      border = `1px solid ${RED}`;
    }

    const buttonStyle = {
      background: backgroundColor,
      border: border,
      color: WHITE,
      cursor: 'pointer',
      ...style
    };

    if (hovering) {
      if (background === 'blue') {
        buttonStyle.background = '#3d71af';
        buttonStyle.color = WHITE;
      } else {
        buttonStyle.background = '#8b0924';
        buttonStyle.color = WHITE;
      }
    }

    if (href) {
      return (
        <a
          href={href}
          style={buttonStyle}
          onClick={onClick}
          onMouseEnter={() => this.setState({ hovering: true })}
          onMouseLeave={() => this.setState({ hovering: false })}
        >
          { children }
        </a>
      );
    }

    return (
      <Button
        fluid={fluid}
        style={buttonStyle}
        onClick={onClick}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
        loading={loading}
      >
        {children}
      </Button>
    )
  }
}


HoverableButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  background: PropTypes.string,
  loading: PropTypes.any,
  href: PropTypes.string
};


export default HoverableButton;
