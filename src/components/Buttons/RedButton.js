import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { DARK_RED, RED, WHITE } from '../../colors';


class RedButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  render() {
    const { hovering } = this.state;
    const { children, fluid, style = {}, disabled, inverted, loading} = this.props;

    const buttonStyle = {
      background: inverted ? 'transparent' : RED,
      border: `1px solid ${RED}`,
      borderBottom: inverted ? `3px solid ${RED}` : `3px solid ${DARK_RED}`,
      color: inverted ? DARK_RED: WHITE,
      borderRadius: 5,
      fontFamily: 'Montserrat',
      ...style
    };

    if (hovering) {
      buttonStyle.background = inverted ? RED : DARK_RED;
      buttonStyle.color = WHITE;
    }

    return (
      <Button
        loading={loading}
        fluid={fluid}
        style={buttonStyle}
        onClick={this.onClick.bind(this)}
        disabled={disabled}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      >
        { children }
      </Button>
    );
  }
}


RedButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool
};


export default RedButton;
