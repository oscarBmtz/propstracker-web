import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button } from 'semantic-ui-react';
import { DARK_GRAY, WHITE } from '../../colors';


class LigthGrayButton extends Component {
  constructor(props) {
    super(props);

    this.state = {
      hovering: false
    };
  }

  onClick() {
    const { loading, onClick } = this.props;

    if (!loading) {
      onClick();
    }
  }

  render() {
    const { hovering } = this.state;
    const { children, fluid, loading, style = {}, disabled, inverted } = this.props;

    const buttonStyle = {
      background: inverted ? 'transparent' : '#DEDEDF',
      border: `1px solid ${'#DEDEDF'}`,
      borderBottom: inverted ? `3px solid ${'#DEDEDF'}` : `3px solid ${DARK_GRAY}`,
      color: inverted ? DARK_GRAY: DARK_GRAY,
      borderRadius: 5,
      fontFamily: 'Montserrat',
      ...style
    };

    if (hovering) {
      buttonStyle.background = inverted ? '#DEDEDF' : DARK_GRAY;
      buttonStyle.color = inverted ? DARK_GRAY : WHITE;
    }

    return (
      <Button
        loading={loading}
        fluid={fluid}
        style={buttonStyle}
        onClick={this.onClick.bind(this)}
        disabled={disabled}
        onMouseEnter={() => this.setState({ hovering: true })}
        onMouseLeave={() => this.setState({ hovering: false })}
      >
        { children }
      </Button>
    );
  }
}


LigthGrayButton.propTypes = {
  children: PropTypes.any,
  onClick: PropTypes.func,
  fluid: PropTypes.bool,
  style: PropTypes.object,
  disabled: PropTypes.bool,
  inverted: PropTypes.bool,
  loading: PropTypes.bool
};


export default LigthGrayButton;
