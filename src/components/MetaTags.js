import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Helmet } from "react-helmet";
import { isEqual } from 'lodash';
import formatter from '../utils/formatter';
import { API_URL, SITE_NAME } from '../utils/constants';


class MetaTags extends Component {
  shouldComponentUpdate(nextProps) {
    const { clean } = formatter;
    return !isEqual(clean(nextProps), clean(this.props));
  }

  render() {
    const { title, description } = this.props;

    let schema;
    let titleTag;
    let twitterTitle;
    let facebookTitle;
    let descriptionTag;
    let twitterDescription;
    let facebookDescription;
    let canonicalTag;
    let facebookURL;
    let twitterImage;
    let facebookImage;
    let facebookImageHeight;
    let facebookImageWidth;

    if (title) {
      titleTag = <title>{ title }</title>;
      twitterTitle = <meta name="twitter:title" content={title} />;
      facebookTitle = <meta property="og:title" content={title} />;
    }

    if (description) {
      descriptionTag = <meta name="description" content={description} />;
      twitterDescription = <meta name="twitter:description" content={description} />;
      facebookDescription = <meta property="og:description" content={description} />;
    }



    return (
      <Helmet>
        { titleTag }
        { descriptionTag }
        { canonicalTag }

        <meta name="twitter:card" content="summary" />
        { twitterTitle }
        { twitterDescription }
        { twitterImage }

        <meta property="og:site_name" content={SITE_NAME} />
        <meta property="og:type" content="article" />
        { facebookTitle }
        { facebookURL }
        { facebookDescription }
        { facebookImage }
        { facebookImageHeight }
        { facebookImageWidth }

        { schema }
      </Helmet>
    );
  }
}


MetaTags.propTypes = {
  title: PropTypes.string,
  description: PropTypes.string
};


export default MetaTags;
