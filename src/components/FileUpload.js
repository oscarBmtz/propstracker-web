import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { WHITE } from '../colors';
import { Input } from 'semantic-ui-react';

class FileUpload extends Component {
  constructor(props){
    super(props);

    this.state = {
      inputName: '',
      base64: null
    }
  }

  onChange(event){
    const currentFile = event.target.files[0];


    if (currentFile && currentFile.type === 'application/pdf') {
      this.convertToBase64(currentFile, (base64) => {
        const value = {
          isValid: true,
          name: this.state.inputName,
          file: base64
        }

        this.setState({ base64 });
        this.props.onChange(value);
      });
    } else {
      const value = {
        isValid: false,
        message: "El archivo no es un PDF"
      }

      this.props.onChange(value);
    }
  };

  convertToBase64(currentFile, callback){
    const reader = new FileReader();
    let base64;

    // asignar callback
    reader.onloadend = () => {
      base64 = reader.result.split(',')[1];
      callback(base64);
    };

    reader.readAsDataURL(currentFile);
  };

  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column' }}>
        <div style={{ display: 'flex', flexDirection: 'column'}}>
          <label style={{ fontWeight: 'bold', marginBottom: 5 }}>
            Nombre del documento
          </label>

          <Input
            value={this.state.inputName}
            style={{ width: '40%', marginBottom: 20 }}
            placeholder="Ingrese nombre del archivo"
            onChange={(e) => {
              this.setState({ inputName: e.target.value });
            }}
            onBlur={(e) => {
              const value = {
                isValid: true,
                name: this.state.inputName,
                file: this.state.base64
              }
              this.props.onChange(value);
            }}
          />
        </div>

        <input
            type="file"
            accept=".pdf"
            onChange={this.onChange.bind(this)}
          />
      </div>
    );
  }
}

FileUpload.propTypes = {
  onChange: PropTypes.func
}

export default FileUpload;
